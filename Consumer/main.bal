import ballerinax/kafka; //very important
import ballerina/log;// not that important

//leave as is, just replace the topic that you have created
kafka:ConsumerConfiguration consumerConfigs = {
    groupId: "group-id",
    // Subscribes to the topics
    topics: ["DSA", "DSA2"],//array

    pollingInterval: 1,
    // Sets the `autoCommit` to `false` so that the records should be committed manually.
    autoCommit: false
};

//add kafka producer here after consumer config
kafka:Producer kafkaProducer = check new (kafka:DEFAULT_URL);

//kafka listener
listener kafka:Listener kafkaListener =
        new (kafka:DEFAULT_URL, consumerConfigs);

service kafka:Service on kafkaListener {
    remote function onConsumerRecord(kafka:Caller caller,
                                kafka:ConsumerRecord[] records) returns error? {
        // The set of Kafka records received by the service are processed one by one.
        foreach var kafkaRecord in records {
            check processKafkaRecord(kafkaRecord);//you can change the name of the function 'processkafkarecord', as long as it has same name as loop 'kafkaRecord'
        }

        // Commits offsets of the returned records by marking them as consumed.
        kafka:Error? commitResult = caller->commit();

        if commitResult is error {
            log:printError("Error occurred while committing the " +
                "offsets for the consumer ", 'error = commitResult);
        }
    }
}

function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) returns error? {
    // The value should be a `byte[]` since the byte[] deserializer is used
    // for the value.
    byte[] value = kafkaRecord.value;

    // Converts the `byte[]` to a `string`.
    string messageContent = check string:fromBytes(value);
// we need to send a message here as a producer
     check kafkaProducer->send({
                                topic: "DSA2",//must be a different topic name
                                value: message.toBytes() });
    log:printInfo("Received Message: " + messageContent);
}


    
