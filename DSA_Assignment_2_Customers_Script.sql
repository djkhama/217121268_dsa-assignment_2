CREATE DATABASE MYSQL_BBE

CREATE TABLE MYSQL_BBE.Customers
            (
            customerId INTEGER NOT NULL AUTO_INCREMENT, 
            firstName VARCHAR(300), 
            lastName  VARCHAR(300), 
            registrationID INTEGER,
            creditLimit DOUBLE, 
            country  VARCHAR(300),
            PRIMARY KEY (customerId)
            )
            
INSERT INTO MYSQL_BBE.Customers
            (firstName, lastName, registrationID,creditLimit,country) VALUES
            ('Peter','Stuart', 1, 5000.75, 'USA')
            
INSERT INTO MYSQL_BBE.Customers
            (firstName, lastName, registrationID,creditLimit,country) VALUES
            ('Dan', 'Brown', 2, 10000, 'UK')
            
SELECT * FROM MYSQL_BBE.Customers

DROP TABLE MYSQL_BBE.Customers

DROP DATABASE MYSQL_BBE